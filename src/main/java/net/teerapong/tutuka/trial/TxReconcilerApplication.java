package net.teerapong.tutuka.trial;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TxReconcilerApplication {

	public static void main(String[] args) {
		SpringApplication.run(TxReconcilerApplication.class, args);
	}
}
