package net.teerapong.tutuka.trial.model

import java.math.BigDecimal
import java.util.*
import net.teerapong.tutuka.trial.service.ComparisonUtil as U;

/**
 * Created by trapongx on 4/24/2016.
 */

data class TxRecord(val rawLine: String, val lineNumber: Int) {
    var valid: Boolean = false;
    var profileName: String? = null;
    var date: Date? = null;
    var amount: BigDecimal? = null;
    var narration: String? = null;
    var description: String? = null;
    var id: String? = null;
    var type: Int? = null;
    var walletReference: String? = null;

    public fun exactMatches(another: TxRecord): Boolean {
        if (!valid || !another.valid)
            return false;
        return U.stringEquals(profileName, another.profileName) &&
                U.dateEquals(date, another.date) &&
                U.bigDecimalEquals(amount, another.amount) &&
                U.stringEquals(narration, another.narration) &&
                U.stringEquals(description, another.description) &&
                U.stringEquals(id, another.id) &&
                U.intEquals(type, another.type) &&
                U.stringEquals(walletReference, another.walletReference);
    }
}