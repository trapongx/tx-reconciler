package net.teerapong.tutuka.trial.model;

/**
 * Created by trapongx on 4/29/2016.
 */
public class CloseMatchesSettings {
    private int maximumTimeDiffSeconds;
    private float maximumAmountDiffUnits;
    private int acceptableDifferencePoint;
    private boolean compromiseWhitespaces;
    private boolean compromiseCaseSensitiveness;
    private boolean compromiseVowelAndSpecialCharacter;

    public int getMaximumTimeDiffSeconds() {
        return maximumTimeDiffSeconds;
    }

    public void setMaximumTimeDiffSeconds(int maximumTimeDiffSeconds) {
        this.maximumTimeDiffSeconds = maximumTimeDiffSeconds;
    }

    public float getMaximumAmountDiffUnits() {
        return maximumAmountDiffUnits;
    }

    public void setMaximumAmountDiffUnits(float maximumAmountDiffUnits) {
        this.maximumAmountDiffUnits = maximumAmountDiffUnits;
    }

    public int getAcceptableDifferencePoint() {
        return acceptableDifferencePoint;
    }

    public void setAcceptableDifferencePoint(int acceptableDifferencePoint) {
        this.acceptableDifferencePoint = acceptableDifferencePoint;
    }

    public boolean isCompromiseWhitespaces() {
        return compromiseWhitespaces;
    }

    public void setCompromiseWhitespaces(boolean compromiseWhitespaces) {
        this.compromiseWhitespaces = compromiseWhitespaces;
    }

    public boolean isCompromiseCaseSensitiveness() {
        return compromiseCaseSensitiveness;
    }

    public void setCompromiseCaseSensitiveness(boolean compromiseCaseSensitiveness) {
        this.compromiseCaseSensitiveness = compromiseCaseSensitiveness;
    }

    public boolean isCompromiseVowelAndSpecialCharacter() {
        return compromiseVowelAndSpecialCharacter;
    }

    public void setCompromiseVowelAndSpecialCharacter(boolean compromiseVowelAndSpecialCharacter) {
        this.compromiseVowelAndSpecialCharacter = compromiseVowelAndSpecialCharacter;
    }
}
