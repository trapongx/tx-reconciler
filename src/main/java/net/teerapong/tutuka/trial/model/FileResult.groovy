package net.teerapong.tutuka.trial.model

/**
 * Created by trapongx on 4/24/2016.
 */
class FileResult {
    public String fileName;
    public int totalRecords;
    public int matchedRecords;
    public int unmatchedRecords;
    public List<TxRecord> unmatchedRecordData;
}
