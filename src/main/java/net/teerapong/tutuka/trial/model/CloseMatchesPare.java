package net.teerapong.tutuka.trial.model;

/**
 * Created by trapongx on 4/29/2016.
 */
public class CloseMatchesPare {
    public TxRecord record1;
    public TxRecord record2;
    public TxRecordSimilarity similarity; // 1 = very likely, 2 = possible
}
