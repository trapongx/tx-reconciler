package net.teerapong.tutuka.trial.model;

/**
 * Created by trapongx on 4/29/2016.
 */
public enum TxRecordSimilarity {
    VERY_CLOSE,
    CLOSE,
    FAR
}
