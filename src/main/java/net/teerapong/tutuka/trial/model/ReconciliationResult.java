package net.teerapong.tutuka.trial.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by trapongx on 4/24/2016.
 */
public class ReconciliationResult {
    public FileResult file1;
    public FileResult file2;
    public List<CloseMatchesPare> closeMatchesPare = new ArrayList<CloseMatchesPare>();
}
