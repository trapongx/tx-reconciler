package net.teerapong.tutuka.trial.controller;

import net.teerapong.tutuka.trial.model.ReconciliationResult;
import net.teerapong.tutuka.trial.service.ReconciliationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * The controller for form submission and result displaying for transactions reconciling.
 * Created by trapongx on 4/23/2016.
 */
@Controller
public class TxReconcilerController {

    @Autowired
    ReconciliationService reconciliationService;

    /**
     * @return template name which is index.html
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index() {
        return "index";
    }

    @RequestMapping(value = "/submit", method = RequestMethod.POST)
    public String handleFormSubmission(@RequestParam MultipartFile file1, @RequestParam MultipartFile file2, RedirectAttributes ra, Model model) {
        if (file1.isEmpty() || file2.isEmpty()) {
            ra.addFlashAttribute("message", "Both input files are required");
            return "redirect:/";
        }

        try {
            ReconciliationResult result = reconciliationService.compare(file1.getInputStream(), file1.getOriginalFilename(), file2.getInputStream(), file2.getOriginalFilename());
            model.addAttribute("result", result);
        } catch (Throwable t) {
            String message = t.getMessage();
            if (message == null)
                message = t.toString();
            ra.addFlashAttribute("message", message);
            return "redirect:/";
        }

        return "result";
    }

}
