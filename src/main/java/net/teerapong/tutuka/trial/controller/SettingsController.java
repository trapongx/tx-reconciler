package net.teerapong.tutuka.trial.controller;

import net.teerapong.tutuka.trial.model.CloseMatchesSettings;
import net.teerapong.tutuka.trial.service.CloseMatchesSettingsManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Map;

/**
 * Created by trapongx on 5/2/2016.
 */
@Controller
public class SettingsController {
    @Autowired
    CloseMatchesSettingsManager closeMatchesSettingsManager;

    @RequestMapping(value = "/settings", method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("closeMatches", closeMatchesSettingsManager.get());
        return "settings";
    }

    @RequestMapping(value = "/settings", method = RequestMethod.POST)
    public String save(CloseMatchesSettings settings,
                       Model model, RedirectAttributes ra) {

        try {
            closeMatchesSettingsManager.update(settings);
            ra.addFlashAttribute("message", "Settings updated");
            return "redirect:settings";
        } catch (Throwable t) {
            String error = t.getMessage();
            if (error == null)
                error = t.toString();
            model.addAttribute("error", "Error, " + error);
            return "settings";
        }
    }
}
