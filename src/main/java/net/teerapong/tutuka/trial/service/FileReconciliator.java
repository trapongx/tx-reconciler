package net.teerapong.tutuka.trial.service;

import net.teerapong.tutuka.trial.model.TxRecord;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by trapongx on 4/24/2016.
 */
public class FileReconciliator {
    private TxRecordBuilder builder;
    private BufferedReader reader;
    private boolean skipHeaderLine = true;
    private boolean headerLineSkipped = false;
    private int lookAheadDistance = 10;
    private int lastMatchedLineNumber = 0;
    private int lineCountSoFar = 0;
    private int matchedCount = 0;
    private List<TxRecord> challengedRecords = new ArrayList<>();
    private List<TxRecord> potentialRecords = new ArrayList<>();
    private boolean isEofReached = false;

    public int getTotalCount() {
        return lineCountSoFar;
    }

    public int getMatchedCount() {
        return matchedCount;
    }

    public List<TxRecord> getChallengedRecords() {
        return challengedRecords;
    }

    public boolean isEofReached() {
        return isEofReached;
    }

    public FileReconciliator(TxRecordBuilder builder, InputStream inputStream, boolean skipHeaderLine, int lookAheadDistance) {
        this.builder = builder;
        this.reader = new BufferedReader(new InputStreamReader(inputStream));
        this.skipHeaderLine = skipHeaderLine;
        this.lookAheadDistance = Integer.max(lookAheadDistance, 10);
    }

    private String readAndSkipBlankLines() throws IOException {
        String line = null;
        while (true) {
            line = reader.readLine();
            if (line == null)
                return null;
            line = line.trim();
            if (line.length() > 0)
                break;
        }
        return line;
    }

    /**
     * @return TxRecord object. If EOF, return null.
     * @throws IOException
     */
    private TxRecord readNextRecord() throws IOException {
        String line = readAndSkipBlankLines();
        if (line != null && skipHeaderLine && !headerLineSkipped) {
            headerLineSkipped = true;
            line = readAndSkipBlankLines();
        }
        if (line == null) {
            isEofReached = true;
            return null;
        }
        lineCountSoFar++;
        return builder.build(line, lineCountSoFar);
    }

    /**
     * Read next line and challenge matching with the other file.
     * Keep doing this loop until EOF or matching does not success within the look ahead distance.
     * After this method return, it will be a chance for the other file to try look back.
     * @return false if EOF reached, otherwise, return true.
     */
    public boolean proceed(FileReconciliator opponent) throws IOException {
        if (isEofReached) {
            return false;
        }
        while (true) {
            TxRecord myRecord;
            if (!potentialRecords.isEmpty()) {
                myRecord = potentialRecords.get(0);
                potentialRecords.remove(0);
            } else {
                myRecord = readNextRecord();
            }
            if (myRecord == null)
                return false;
            if (myRecord.getValid() && opponent.challenge(myRecord)) {
                matchedCount++;
                continue;
            } else {
                challengedRecords.add(myRecord);
                return true;
            }
        }
    }

    /**
     * @param recordFromAnotherFile
     * @return true if matched, else false.
     */
    public boolean challenge(TxRecord recordFromAnotherFile) throws IOException {
        int lookAheadCount = 0;
        for (int i=0; i < potentialRecords.size(); i++) {
            TxRecord rec = potentialRecords.get(i);
            if (rec.exactMatches(recordFromAnotherFile)) {
                matchedCount++;
                potentialRecords.remove(i);
                return true;
            }
            if (++lookAheadCount >= lookAheadDistance)
                break;
        }
        while(lookAheadCount < lookAheadDistance) {
            TxRecord rec = readNextRecord();
            if (rec == null)
                return false;
            if (rec.exactMatches(recordFromAnotherFile)) {
                matchedCount++;
                return true;
            } else {
                potentialRecords.add(rec);
                if (++lookAheadCount >= lookAheadDistance)
                    break;
            }
        }
        return false;
    }

    public void close() {
        if (reader != null) {
            try {
                reader.close();
            } catch (Throwable t) {

            }
        }
    }
}

