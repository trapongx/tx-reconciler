package net.teerapong.tutuka.trial.service;

/**
 * Created by trapongx on 5/2/2016.
 */
public interface Stringifier {
    String stringify(Object o);
}
