package net.teerapong.tutuka.trial.service;

import net.teerapong.tutuka.trial.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by trapongx on 4/24/2016.
 */
@Service
public class ReconciliationService {

    @Autowired
    TxRecordBuilder builder;

    @Autowired
    CloseMatchesSettingsManager closeMatchesSettingsManager;

    public ReconciliationResult compare(InputStream fileInputStream1, String fileName1, InputStream fileInputStream2, String fileName2) throws IOException {
        FileReconciliator reconciliator1 = new FileReconciliator(builder, fileInputStream1, true, 10);
        FileReconciliator reconciliator2 = new FileReconciliator(builder, fileInputStream2, true, 10);

        FileReconciliator remember1 = reconciliator1;
        FileReconciliator remember2 = reconciliator2;

        try {
            while (true) {
                boolean eof = !reconciliator1.proceed(reconciliator2);
                if (eof && reconciliator2.isEofReached()) {
                    break;
                }
                if (!reconciliator2.isEofReached()) {
                    //Swap turn
                    FileReconciliator tmp = reconciliator1;
                    reconciliator1 = reconciliator2;
                    reconciliator2 = tmp;
                }
            }
        } finally {
            reconciliator1.close();
            reconciliator2.close();
        }

        reconciliator1 = remember1;
        reconciliator2 = remember2;

        ReconciliationResult result = new ReconciliationResult();
        result.file1 = buildResult(reconciliator1, fileName1);
        result.file2 = buildResult(reconciliator2, fileName2);

        for (TxRecord rec1 : result.file1.unmatchedRecordData) {
            TxRecord matchedRec = null;
            TxRecordSimilarity similarity = TxRecordSimilarity.FAR;
            for (TxRecord rec2 : result.file2.unmatchedRecordData) {
                similarity = isCloseMatched(rec1, rec2);
                if (similarity != TxRecordSimilarity.FAR) {
                    matchedRec = rec2;
                    break;
                }
            }
            if (matchedRec != null) {
                CloseMatchesPare pare = new CloseMatchesPare();
                pare.record1 = rec1;
                pare.record2 = matchedRec;
                pare.similarity = similarity;
                result.closeMatchesPare.add(pare);
                result.file2.unmatchedRecordData.remove(matchedRec);
            }
        }

        for (CloseMatchesPare pare : result.closeMatchesPare) {
            result.file1.unmatchedRecordData.remove(pare.record1);
        }

        result.file1.unmatchedRecords = result.file1.unmatchedRecordData.size();
        result.file2.unmatchedRecords = result.file2.unmatchedRecordData.size();

        return result;
    }

    private FileResult buildResult(FileReconciliator reconciliator, String fileName) {
        FileResult result = new FileResult();
        result.fileName = fileName;
        result.totalRecords =  reconciliator.getTotalCount();
        result.matchedRecords = reconciliator.getMatchedCount();
        result.unmatchedRecords = reconciliator.getChallengedRecords().size();
        result.unmatchedRecordData = reconciliator.getChallengedRecords();
        return result;
    }

    /**
     *
     * @param rec1
     * @param rec2
     * @return 0 if not. 1 if very likely. 2 if possible
     */
    TxRecordSimilarity isCloseMatched(TxRecord rec1, TxRecord rec2) {
        CloseMatchesSettings settings = closeMatchesSettingsManager.get();

        int differentPoint = 0;

        // Check datetime
        if (rec1.getDate() != null && rec2.getDate() != null) {
            long t1 = rec1.getDate().getTime();
            long t2 = rec2.getDate().getTime();
            if (t1 != t2) {
                if (Math.abs(t1 - t2) > settings.getMaximumTimeDiffSeconds() * 1000)
                    differentPoint += 5;
                else
                    differentPoint += 1;
            }
        } else {
            differentPoint += 5;
        }
        if (differentPoint > settings.getAcceptableDifferencePoint())
            return TxRecordSimilarity.FAR;

        // Check amount
        if (rec1.getAmount() != null && rec2.getAmount() != null) {
            if (!rec1.getAmount().equals(rec2.getAmount())) {
                if (rec1.getAmount().subtract(rec2.getAmount()).abs().floatValue() > settings.getMaximumAmountDiffUnits())
                    differentPoint += 5;
                else
                    differentPoint += 1;
            }
        } else {
            differentPoint += 5;
        }
        if (differentPoint > settings.getAcceptableDifferencePoint())
            return TxRecordSimilarity.FAR;

        // Check type
        if (rec1.getType() == null || rec2.getType() == null || !rec1.getType().equals(rec2.getType())) {
            differentPoint += 5;
        }
        if (differentPoint > settings.getAcceptableDifferencePoint())
            return TxRecordSimilarity.FAR;

        // Check all string fields
        // All string fields must be considered matched after compromising. Else, two records are considered not close matched.

        String[][] stringToCompares = new String[][] {
                {rec1.getProfileName(), rec2.getProfileName()},
                {rec1.getNarration(), rec2.getNarration()},
                {rec1.getDescription(), rec2.getDescription()},
                {rec1.getId(), rec2.getId()},
                {rec1.getWalletReference(), rec2.getWalletReference()}
        };
        for (int i=0; i < stringToCompares.length; i++) {
            String s1 = stringToCompares[i][0];
            String s2 = stringToCompares[i][1];
            if (s1 != null && s2 != null) {
                if (!s1.equals(s2)) {
                    if (!isStringSimilar(s1, s2, settings))
                        differentPoint += 5;
                    else
                        differentPoint += 1;
                }
            } else {
                differentPoint += 5;
            }
            if (differentPoint > settings.getAcceptableDifferencePoint())
                return TxRecordSimilarity.FAR;
        }

        if (differentPoint <= 1)
            return TxRecordSimilarity.VERY_CLOSE;
        else
        return TxRecordSimilarity.CLOSE;
   }

    boolean isStringSimilar(String s1, String s2, CloseMatchesSettings settings) {
        if (s1 == null || s2 == null)
            return false;
        s1 = s1.trim();
        s2 = s2.trim();
        if (s1.equals(s2))
            return true;
        if (settings.isCompromiseCaseSensitiveness()) {
            s1 = s1.toLowerCase();
            s2 = s2.toLowerCase();
        }
        if (settings.isCompromiseWhitespaces()) {
            s1 = s1.replaceAll("\\s{1,}", " ");
            s2 = s2.replaceAll("\\s{1,}", " ");
        }
        if (settings.isCompromiseVowelAndSpecialCharacter()) {
            s1 = s1.replaceAll("([aeiouAEIOU]|[^a-zA-Z0-9]){1,}", "");
            s2 = s2.replaceAll("([aeiouAEIOU]|[^a-zA-Z0-9]){1,}", "");
        }
        return s1.equals(s2);
    }
}
