package net.teerapong.tutuka.trial.service;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by trapongx on 4/24/2016.
 */
public class ComparisonUtil {
    public static boolean notNullBoth(Object o1, Object o2) {
        return o1 != null && o2 != null;
    }

    public static boolean intEquals(Integer i1, Integer i2) {
        return notNullBoth(i1, i2) && i1.equals(i2);
    }

    public static boolean stringEquals(String s1, String s2) {
        return notNullBoth(s1, s2) && s1.equals(s2);
    }

    public static boolean dateEquals(Date d1, Date d2) {
        return notNullBoth(d1, d2) && d1.equals(d2);
    }

    public static boolean bigDecimalEquals(BigDecimal big1, BigDecimal big2) {
        return notNullBoth(big1, big2) && big1.equals(big2);
    }
}
