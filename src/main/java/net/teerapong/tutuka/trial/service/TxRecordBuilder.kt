package net.teerapong.tutuka.trial.service

import net.teerapong.tutuka.trial.model.TxRecord
import org.springframework.stereotype.Service
import java.math.BigDecimal
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by trapongx on 4/24/2016.
 */
@Service
class TxRecordBuilder {
    val dateFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    fun build(rawLine: String, lineNumber: Int) : TxRecord {
        var fields = rawLine.split(",").map { it.trim() };
        for (i in (fields.size-1) downTo 0) {
            if (fields[i].length > 0) {
                if (i < fields.size-1){
                    fields = fields.subList(0, i + 1);
                }
                break;
            }
        }

        val tx = TxRecord(rawLine, lineNumber);

        var i = -1;
        var allFieldValid = true;
        if (++i <= fields.size-1) {
            val (b, v) = readString(fields, i);
            allFieldValid = allFieldValid and (b as Boolean);
            if (v != null)
                tx.profileName = v as String;
        }
        if (++i <= fields.size-1) {
            val (b, v) = readDate(fields, i, dateFormat);
            allFieldValid = allFieldValid and (b as Boolean);
            if (v != null)
                tx.date = v as Date;
        }
        if (++i <= fields.size-1) {
            val (b, v) = readBigDecimal(fields, i);
            allFieldValid = allFieldValid and (b as Boolean);
            if (v != null)
                tx.amount = v as BigDecimal;
        }
        if (++i <= fields.size-1) {
            val (b, v) = readString(fields, i);
            allFieldValid = allFieldValid and (b as Boolean);
            if (v != null)
                tx.narration = v as String;
        }
        if (++i <= fields.size-1) {
            val (b, v) = readString(fields, i);
            allFieldValid = allFieldValid and (b as Boolean);
            if (v != null)
                tx.description = v as String;
        }
        if (++i <= fields.size-1) {
            val (b, v) = readString(fields, i);
            allFieldValid = allFieldValid and (b as Boolean);
            if (v != null)
                tx.id = v as String;
        }
        if (++i <= fields.size-1) {
            val (b, v) = readInt(fields, i);
            allFieldValid = allFieldValid and (b as Boolean);
            if (v != null)
                tx.type = v as Int;
        }
        if (++i <= fields.size-1) {
            val (b, v) = readString(fields, i);
            allFieldValid = allFieldValid and (b as Boolean);
            if (v != null)
                tx.walletReference = v as String;
        }

        tx.valid = allFieldValid;

        return tx;
    }

    private fun readString(fields: List<String>, index: Int): List<Any?> {
        if (index > fields.size-1)
            return listOf(false, null);
        return listOf(true, fields[index]);
    }

    private fun readDate(fields: List<String>, index: Int, fmt: DateFormat): List<Any?> {
        if (index > fields.size-1)
            return listOf(false, null);
        var d: Date? = try { fmt.parse(fields[index]); } catch (e: ParseException) { null };
        if (d == null)
            return listOf(false, null);
        else
            return listOf(true, d);
    }

    private fun readBigDecimal(fields: List<String>, index: Int): List<Any?> {
        if (index > fields.size-1)
            return listOf(false, null);
        var d: BigDecimal? = try { BigDecimal(fields[index]); } catch (e: ParseException) { null };
        if (d == null)
            return listOf(false, null);
        else
            return listOf(true, d);
    }

    private fun readInt(fields: List<String>, index: Int): List<Any?> {
        if (index > fields.size-1)
            return listOf(false, null);
        var d: Int? = try { Integer.parseInt(fields[index]); } catch (e: ParseException) { null };
        if (d == null)
            return listOf(false, null);
        else
            return listOf(true, d);
    }
}