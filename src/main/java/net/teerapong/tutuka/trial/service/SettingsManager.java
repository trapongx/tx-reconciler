package net.teerapong.tutuka.trial.service;

import org.springframework.stereotype.Service;

import java.io.*;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by trapongx on 5/2/2016.
 */
@Service
public class SettingsManager implements Runnable {
    private static final Logger logger = Logger.getLogger(SettingsManager.class.getName());

    private volatile Properties props;
    private volatile long lastTimeUpdated;
    private volatile boolean dirty;
    private volatile Thread writerThread;

    private Properties getProps() {
        try {
            if (props == null) {
                props = new Properties();
                File file = new File("settings.properties");
                if (file.exists()) {
                    props.load(new FileInputStream(file));
                }
            }
            return props;
        } catch (Throwable t) {
            throw new RuntimeException(t);
        }
    }

    private void markDirty() {
        lastTimeUpdated = System.currentTimeMillis();
        dirty = true;
        if (writerThread == null) {
            writerThread = new Thread(this);
            writerThread.start();
        }
    }

    public void run() {
        // TODO Current implementation does not do retry.
        // Should be improved by retrying in an exponential increased delay time and limited number of retry.
        while (dirty) {
            long tm = System.currentTimeMillis() - lastTimeUpdated;
            if (tm > 5 * 1000) {
                try {
                    writeToFile();
                    logger.fine("Wrote settings to file at");
                } catch (Throwable t) {
                    logger.log(Level.SEVERE, "Failed to save settings to file. All settings may be lost. Will not retry.", t);
                } finally {
                    dirty = false;
                    lastTimeUpdated = 0;
                }
            } else {
                tm = 5 * 1000 - tm;
                if (tm > 0) {
                    try {
                        Thread.sleep(tm);
                    } catch (Throwable t) {
                        // Ignore
                    }
                }
            }
        }
        // The way to let #markDirty() know that no thread is responsible for writing, it need another one.
        writerThread = null;
    }

    private void writeToFile() throws IOException {
        File file = new File("config/settings.properties");
        if (!file.exists()) {
            file.mkdirs();
        }
        props.store(new FileOutputStream("settings.properties"), "update");
    }

    public Object get(String key, Stringifier stringifier, Destringifier destringifier, Object defaultValue) {
        String v = getProps().getProperty(key);
        if (v == null) {
            set(key, stringifier, defaultValue);
            return defaultValue;
        } else {
            return destringifier.destringify(v);
        }
    }

    public void set(String key, Stringifier stringifier, Object value) {
        String v = getProps().getProperty(key);
        String s = stringifier.stringify(value);
        if (v == null || !v.equals(s)) {
            getProps().setProperty(key, s);
            markDirty();
        }
    }

    public long getLong(String key, long defaultValue) {
        return ((Long) get(key, (v) -> v.toString(), (s) -> Long.valueOf(s), defaultValue)).longValue();
    }

    public int getInt(String key, int defaultValue) {
        return (int) getLong(key, defaultValue);
    }

    public double getDouble(String key, double defaultValue) {
        return ((Double) get(key, (v) -> v.toString(), (s) -> Double.valueOf(s), defaultValue)).doubleValue();
    }

    public float getFloat(String key, float defaultValue) {
        return (float) getDouble(key, defaultValue);
    }

    public boolean getBoolean(String key, boolean defaultValue) {
        return ((Boolean) get(key, (v) -> v.toString(), (s) -> s != null && "|TRUE|true|1|".contains(s), defaultValue)).booleanValue();
    }

    public void setLong(String key, long value) {
        set(key, (v) -> v.toString(), Long.valueOf(value));
    }

    public void setInt(String key, int value) {
        setLong(key, value);
    }

    public void setDouble(String key, double value) {
        set(key, (v) -> v.toString(), Double.valueOf(value));
    }

    public void setFloat(String key, float value) {
        setDouble(key, value);
    }

    public void setBoolean(String key, boolean value) {
        set(key, (v) -> v.toString(), Boolean.valueOf(value));
    }

}
