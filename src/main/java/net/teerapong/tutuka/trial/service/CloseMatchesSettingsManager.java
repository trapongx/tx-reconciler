package net.teerapong.tutuka.trial.service;

import net.teerapong.tutuka.trial.model.CloseMatchesSettings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by trapongx on 4/29/2016.
 */
@Service
public class CloseMatchesSettingsManager {
    private volatile CloseMatchesSettings instance;

    @Autowired
    SettingsManager settingsManager;

    public CloseMatchesSettings get() {
        if (instance == null) {
            instance = new CloseMatchesSettings();
            instance.setMaximumTimeDiffSeconds(settingsManager.getInt("closeMatches.maximumTimeDiffSeconds", 60));
            instance.setMaximumAmountDiffUnits(settingsManager.getFloat("closeMatches.maximumAmountDiffUnits", 0.1f));
            instance.setAcceptableDifferencePoint(settingsManager.getInt("closeMatches.acceptableDifferencePoint", 6));
            instance.setCompromiseWhitespaces(settingsManager.getBoolean("closeMatches.compromiseWhitespaces", true));
            instance.setCompromiseCaseSensitiveness(settingsManager.getBoolean("closeMatches.compromiseCaseSensitiveness", true));
            instance.setCompromiseVowelAndSpecialCharacter(settingsManager.getBoolean("closeMatches.compromiseVowelAndSpecialCharacter", true));
        }
        return instance;
    }

    public void update(CloseMatchesSettings newSettings) {
        settingsManager.setInt("closeMatches.maximumTimeDiffSeconds", newSettings.getMaximumTimeDiffSeconds());
        settingsManager.setFloat("closeMatches.maximumAmountDiffUnits", newSettings.getMaximumAmountDiffUnits());
        settingsManager.setInt("closeMatches.acceptableDifferencePoint", newSettings.getAcceptableDifferencePoint());
        settingsManager.setBoolean("closeMatches.compromiseWhitespaces", newSettings.isCompromiseWhitespaces());
        settingsManager.setBoolean("closeMatches.compromiseCaseSensitiveness", newSettings.isCompromiseCaseSensitiveness());
        settingsManager.setBoolean("closeMatches.compromiseVowelAndSpecialCharacter", newSettings.isCompromiseVowelAndSpecialCharacter());
        this.instance = newSettings;
    }
}
